import { IsinMockPage } from './app.po';

describe('isin-mock App', function() {
  let page: IsinMockPage;

  beforeEach(() => {
    page = new IsinMockPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
