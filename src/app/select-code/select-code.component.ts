import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'isin-select-code',
  templateUrl: './select-code.component.html',
  styleUrls: ['./select-code.component.css']
})
export class SelectCodeComponent implements OnInit {
  @Input('parentFormGroup') public parentFormGroup: FormGroup;

  public selectCode: string;

  constructor() { }

  ngOnInit() {
    this.reset();
  }

  reset(){
    this.selectCode = "A";
  }

}
