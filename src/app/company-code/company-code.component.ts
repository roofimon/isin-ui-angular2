import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { ISINSearchServiceService } from '../isinsearch-service.service';

@Component({
  selector: 'isin-company-code',
  templateUrl: './company-code.component.html',
  styleUrls: ['./company-code.component.css']
})
export class CompanyCodeComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  @Input('companyCode') public companyCode:string;
  public companyCodeDesc:string;
  public companyName:string;
  public isDisabled: boolean = false;

  results = [];
  columns: Array<any> = [
    {
      title: 'Company Code', name: 'companyID'
    },
    {
      title: 'Company Name', name: 'issuerNameEN'
    }
  ];

  constructor(private fb:FormBuilder, private isinSearchService: ISINSearchServiceService) { }

  ngOnInit() {
    if(!this.parentFormGroup) {
      this.parentFormGroup = this.fb.group({
         companyCode: ['']
        }); 
    }
    this.reset();
  }

  reset(){
    this.companyCode = "";
    this.companyCodeDesc = "";
    this.companyName = "";
  }

  public lookupCompanyByCodeOnBlur(){
    if(this.companyCode !== undefined && this.companyCode !== ""){
      this.isinSearchService.lookupCompanyByCode(this.companyCode)
          .subscribe(results =>{
            this.results = results;
            if(this.results.length == 1){
              this.companyCode = this.results[0].companyID;
              this.companyCodeDesc = this.results[0].issuerNameEN;
            }else if(this.results.length > 1){
              //Open Modal
              console.log(this.results);
            }else{
              this.companyCodeDesc = "Not Found";
            }
          })
      }else{
        this.companyCode = "";
        this.companyCodeDesc = "";
      }
  }

  lookupCompanyByModal(){
    this.isinSearchService.lookupCompanyByModal(this.companyName)
          .subscribe(results =>{
            this.results = results;
          });
  }

  clearSearchResultData(){
    this.results = [];
    this.companyName = "";
  }

  handleEvent(data: any) {
    this.companyCode = data.row.companyID;
    this.companyCodeDesc = data.row.issuerNameEN;
  }

}
