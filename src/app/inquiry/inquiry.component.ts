import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'isin-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.css']
})
export class InquiryComponent implements OnInit {

  public fg: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.fg = this.fb.group({
      securitySymbol: ['', [Validators.required, Validators.maxLength(32)]],
      securityType: ['', [Validators.required, Validators.maxLength(30)]],
      companyCode: ['', [Validators.required, Validators.maxLength(4)]],
    });
  }

}
