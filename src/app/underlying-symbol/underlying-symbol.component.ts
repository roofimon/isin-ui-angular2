import { Component, OnInit, Input } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'isin-underlying-symbol',
  templateUrl: './underlying-symbol.component.html',
  styleUrls: ['./underlying-symbol.component.css']
})
export class UnderlyingSymbolComponent implements OnInit {

  @Input('parentFormGroup') parentFormGroup: FormGroup;
  @Input() securityType: string;
  
  public underlyingSymbol: string;
  public underlyingSymbolDesc: string;
  public underlyingCode: string;
  public underlyingName: string;
  public results =[];
  columns: Array<any> = [
    {
      title: 'Underlying Symbol', name: 'symbol'
    },
    {
      title: 'Underlying Name', name: 'name'
    }
  ];
  selectedRow: any;

  constructor(private isinSearchService: ISINSearchServiceService) { }

  ngOnInit() {
    this.underlyingSymbol = "";
    this.underlyingSymbolDesc = "";
    this.underlyingCode = "";
    this.underlyingName = "";
  }

  clearSearchResultData(){
    this.results = [];
    this.underlyingCode = "";
    this.underlyingName = "";
  }

  public lookupUnderlyingByModal(){
    this.isinSearchService.lookUpUnderlying(this.underlyingCode, this.underlyingName, this.securityType)
      .subscribe(results => {
        this.results = results;
      });
  }

  lookupUnderlyingSymbolOnBlur() {
    if(this.underlyingSymbol !== ""){
      this.isinSearchService.lookUpUnderlying(this.underlyingSymbol, "", this.securityType)
      .subscribe(results => {
        this.results = results;
        if(this.results.length == 1){
            this.underlyingSymbol = this.results[0].symbol;
            this.underlyingSymbolDesc = this.results[0].name;
          }
      });
    };
  }

  handleEvent(data: any) {
    this.underlyingSymbol = data.row.symbol;
    this.underlyingSymbolDesc = data.row.name;
  }

}
