import { Component, OnInit, Input } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'isin-future-option-type',
  templateUrl: './future-option-type.component.html',
  styleUrls: ['./future-option-type.component.css']
})
export class FutureOptionTypeComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  public futureOptionType:string;
  results = [];
  constructor(private isinSearchServiceService:ISINSearchServiceService) { }

  ngOnInit() {
    this.listFutureOptionType();
    this.futureOptionType = "A";
  }

  public listFutureOptionType(){
    this.isinSearchServiceService.listFutureOption()
        .subscribe(results => {
          this.results = results;
    });
  }

}
