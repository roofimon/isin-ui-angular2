/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { JumpToPageComponent } from './jump-to-page.component';

describe('JumpToPageComponent', () => {
  let component: JumpToPageComponent;
  let fixture: ComponentFixture<JumpToPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JumpToPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JumpToPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
