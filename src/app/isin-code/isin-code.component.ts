import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'isin-isin-code',
  templateUrl: './isin-code.component.html',
  styleUrls: ['./isin-code.component.css']
})
export class IsinCodeComponent implements OnInit {

  isinCodeLocalOrg:string;
  isinCodeForeignOrg:string;
  isinCodeNVDROrg:string;
  isinCodeTTFOrg:string;

  constructor() { }

  ngOnInit() {
  }

}
