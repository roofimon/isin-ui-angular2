import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/modal';
import { InquiryEntryUpdateComponent } from '../inquiry-entry-update/inquiry-entry-update.component';

@Component({
  selector: 'isin-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.css']
})
export class ModalConfirmComponent implements OnInit {

  @ViewChild('confirmModal') confirmModal:ModalDirective;
  @ViewChild('modalConfirmBody') modalConfirmBody:ElementRef;

  public title:string;

  constructor(private inquiryEntryUpdateComponent : InquiryEntryUpdateComponent) { }

  ngOnInit() {
    
  }

  show(title:string, content:string) {
    this.title = title;
    this.modalConfirmBody.nativeElement.innerHTML = content;
    this.confirmModal.show();
  }

  submitConfirm(){
    this.confirmModal.hide();
    this.inquiryEntryUpdateComponent.createCFIISIN();
  }

}
