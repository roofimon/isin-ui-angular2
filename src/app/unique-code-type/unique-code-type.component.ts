import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ISINSearchServiceService } from '../isinsearch-service.service';

@Component({
  selector: 'isin-unique-code-type',
  templateUrl: './unique-code-type.component.html',
  styleUrls: ['./unique-code-type.component.css']
})
export class UniqueCodeTypeComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  public uniqueCodeType: string;
  results = [];

  constructor(private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    this.listUniqueCodeType();
    this.uniqueCodeType = "";
  }

  public listUniqueCodeType(): void {
    this.isinSearchServiceService.listUniqueCode()
      .subscribe(results => {
        this.results = results;
      });
  }

  reset(){
    this.uniqueCodeType = "";
  }

}
