import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ISINSearchServiceService, EntryUpdateParam } from '../isinsearch-service.service';
import { ModalConfirmComponent } from '../modal-confirm/modal-confirm.component';
import { SecurityTypeComponent } from '../security-type/security-type.component';
import { CompanyCodeComponent } from '../company-code/company-code.component';
import { SelectCodeComponent } from '../select-code/select-code.component';
import { SecuritySymbolComponent } from '../security-symbol/security-symbol.component';
import { Router } from '@angular/router';

@Component({
  selector: 'isin-inquiry-entry-update',
  templateUrl: './inquiry-entry-update.component.html',
  styleUrls: ['./inquiry-entry-update.component.css']
})
export class InquiryEntryUpdateComponent implements OnInit, AfterViewInit {
  public fg: FormGroup;

  @ViewChild('modalConfirm') modalConfirm;
  @ViewChild('modalMessage') modalMessage;
  
  @ViewChild('securitySymbolComp') securitySymbolComp;
  @ViewChild('securityTypeComp') securityTypeComp;
  @ViewChild('companyCodeComp') companyCodeComp;
  @ViewChild('selectCodeComp') selectCodeComp;

  results = [];
  columnsAll: Array<any> = [
    {
      title: 'Select', name: 'select'
    },
    {
      title: 'Security Symbol', name: 'securityAbbr' //sort: 'asc', filtering: {filterString: '', placeholder: 'Filter by name'}
    },
    {
      title: 'Company Name', name: 'companyName', //sort: false, filtering: {filterString: '', placeholder: 'Filter by position'}
    },
    {
      // title: 'Effective Date', className: ['office-header', 'text-success'], name: 'effectiveDate', sort: 'asc'
      title: 'Effective Date', name: 'effectiveDate'
    },
    {
      title: 'ISIN Code Local', name: 'isinCodeLocal', //, filtering: {filterString: '', placeholder: 'Filter by extn.'}
    },
    {
      // title: 'ISIN Code Foreign', className: 'text-warning', name: 'isinCodeForeign'
      title: 'ISIN Code Foreign', name: 'isinCodeForeign'
    },
    {
      title: 'ISIN Code NVDR', name: 'isinCodeNVDR'
    },
    {
      title: 'ISIN Code TTF', name: 'isinCodeTTF'
    },
    {
      title: 'CFI Code', name: 'cfiCode'
    },
    {
      title: 'CFI Code NVDR', name: 'cfiCodeNVDR'
    }
  ];
  columnsISIN: Array<any> = [
    {
      title: 'Select', name: 'select'
    },
    {
      title: 'Security Symbol', name: 'securityAbbr'
    },
    {
      title: 'Company Name', name: 'companyName'
    },
    {
      title: 'Effective Date', name: 'effectiveDate'
    },
    {
      title: 'ISIN Code Local', name: 'isinCodeLocal'
    },
    {
      title: 'ISIN Code Foreign', name: 'isinCodeForeign'
    },
    {
      title: 'ISIN Code NVDR', name: 'isinCodeNVDR'
    },
    {
      title: 'ISIN Code TTF', name: 'isinCodeTTF'
    }
  ];
  columnsCFI: Array<any> = [
    {
      title: 'Select', name: 'select'
    },
    {
      title: 'Security Symbol', name: 'securityAbbr'
    },
    {
      title: 'Company Name', name: 'companyName'
    },
    {
      title: 'Effective Date', name: 'effectiveDate'
    },
    {
      title: 'CFI Code', name: 'cfiCode'
    },
    {
      title: 'CFI Code NVDR', name: 'cfiCodeNVDR'
    }
  ];

  public selectableColumns: Array<any> = ['select'];

  public securityAbbr:string;
  public securityType:string;
  public companyCode:string;
  public selectCode:string;

  public constructor(
    private isinSearchServiceService: ISINSearchServiceService,
    private router:Router,
    private fb:FormBuilder
  ) {}

  ngOnInit() {
    this.fg = this.fb.group({
      securitySymbol: [],
      securityType: [],
      companyCode: [],
      selectCode: [],
    }); 
  }

  ngAfterViewInit() {
    let entryUpdateParam = this.isinSearchServiceService.getEntryUpdateParam();
    if (entryUpdateParam) {
      this.securitySymbolComp.securityAbbr = entryUpdateParam.securityAbbr;
      this.securitySymbolComp.lookupSecurityByAbbrOnBlur();      
      this.securityTypeComp.securityType = entryUpdateParam.securityType;
      this.companyCodeComp.companyCode = entryUpdateParam.companyCode;
      this.companyCodeComp.lookupCompanyByCodeOnBlur();
      this.selectCodeComp.selectCode = entryUpdateParam.selectCode;

      this.reSearchSecurityInfo(this.securitySymbolComp.securityAbbr, this.securityTypeComp, this.companyCodeComp.companyCode, this.selectCodeComp.selectCode);

    }  
  }


  public searchSecurityInfo(securitySymbol:string, securityComp:SecurityTypeComponent, companyCode:string, selectCode:string): void {
    this.isinSearchServiceService.getSecurityInfo(securitySymbol, securityComp.securityType, companyCode)
      .subscribe(results => {
        this.results = results;
        this.selectCode = selectCode;

        if (this.isDataNotFound(this.results) && securitySymbol) {

          if (this.selectCode == "C") {
            let content = `The system cannot find ISIN data for: <br/>
                          <br/>
                          Security Symbol : ${securitySymbol} <br/>
                          <br/>
                          Please entry ISIN data first by choosing 'ISIN Code',
                          or you can entry both ISIN and CFI data by choosing 'All'`;
            this.modalMessage.show("ISIN Data Not Found",content);

          } else {
            let securityTypeDesc:string;
            securityComp.results.forEach(obj => {
              if (obj.code==securityComp.securityType) securityTypeDesc=obj.type;
            });

            let content = `The system cannot find ISIN/CFI data for: <br/>
                          <br/>
                          Security Symbol : ${securitySymbol} <br/>
                          Security Type : ${securityComp.securityType} ${securityTypeDesc} <br/>
                          Company Code : ${companyCode} <br/>
                          <br/>
                          Do you want to confirm the entry of ISIN/CFI data for the above security?`;
            this.modalConfirm.show("Entry Confirmation",content);

            this.securityAbbr = securitySymbol;
            this.securityType = securityComp.securityType;
            this.companyCode = companyCode;
          }

        } else {
          this.results.forEach(result => {
            result["select"] = "Select";
          });
        }
      });
  }

  public reSearchSecurityInfo(securitySymbol:string, securityComp:SecurityTypeComponent, companyCode:string, selectCode:string): void {
    this.isinSearchServiceService.getSecurityInfo(securitySymbol, securityComp.securityType, companyCode)
      .subscribe(results => {
        this.results = results;
        this.selectCode = selectCode;

        if (this.isDataFound(this.results)) {
          this.results.forEach(result => {
            result["select"] = "Select";
          });
        }
      });
  }

  private isDataFound(results:Array<any>) {
    return results.length > 0;
  }

  private isDataNotFound(results:Array<any>){
    return results.length == 0;
  }

  public resetSearchForm(securitySymbolComp:SecuritySymbolComponent,
                        securityTypeComp:SecurityTypeComponent,
                        companyCodeComp:CompanyCodeComponent,
                        selectCodeComp:SelectCodeComponent): void {
    this.results = [];
    securitySymbolComp.reset();
    securityTypeComp.reset();
    companyCodeComp.reset();
    selectCodeComp.reset();
  }

  public createCFIISIN(){
    let entryUpdateParam = new EntryUpdateParam();    
    entryUpdateParam.securityAbbr = this.securityAbbr;
    entryUpdateParam.securityType = this.securityType;
    entryUpdateParam.companyCode = this.companyCode;
    entryUpdateParam.selectCode = this.selectCodeComp.selectCode;
    this.isinSearchServiceService.setEntryUpdateParam(entryUpdateParam);

    this.router.navigate(['/entryUpdate']);
  }

  public updateCFIISIN(event) {
    let entryUpdateParam = new EntryUpdateParam();
    entryUpdateParam.securityId = event.row.securityId;
    entryUpdateParam.securityAbbr = this.securitySymbolComp.securityAbbr;
    entryUpdateParam.securityType = this.securityTypeComp.securityType;
    entryUpdateParam.selectCode = this.selectCodeComp.selectCode;
    this.isinSearchServiceService.setEntryUpdateParam(entryUpdateParam);

    this.router.navigate(['/entryUpdate']);
  }

}
