import { Component } from '@angular/core';
import { ISINSearchServiceService } from './isinsearch-service.service';

@Component({
  providers: [ISINSearchServiceService],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  public constructor(private isinSearchServiceService: ISINSearchServiceService) {}
}
