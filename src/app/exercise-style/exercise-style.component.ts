import { Component, OnInit, Input } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'isin-exercise-style',
  templateUrl: './exercise-style.component.html',
  styleUrls: ['./exercise-style.component.css']
})
export class ExerciseStyleComponent implements OnInit {

  @Input('parentFormGroup') parentFormGroup: FormGroup;
  public exerciseStyle: string;
  results = [];

  constructor(private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    this.exerciseStyle = "";
    this.listDWExerciseStyle();
  }

  public listDWExerciseStyle(): void {
    this.isinSearchServiceService.listDWExerciseStyle()
      .subscribe(results => {
        this.results = results;
      });
  }

}
