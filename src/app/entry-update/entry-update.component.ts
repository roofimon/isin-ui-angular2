import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  providers: [],
  selector: 'isin-entry-update',
  templateUrl: './entry-update.component.html',
  styleUrls: ['./entry-update.component.css']
})
export class EntryUpdateComponent implements OnInit, AfterViewInit {

  public fg: FormGroup;

  public securityId:number;
  public securityAbbr:string;
  public securityType:string;
  public companyCode:string;
  public selectCode:string;

  uniqueCode:string;
  issuerNameThai:string;
  issuerNameEnglish:string;
  securityNameThai:string;
  securityNameEnglish:string;
  parValue:string;

  @ViewChild('securityTypeComp') securityTypeComp;
  @ViewChild('marketComp') marketComp;
  @ViewChild('companyCodeComp') companyCodeComp;
  @ViewChild('uniqueCodeComp') uniqueCodeComp;
  @ViewChild('currencyCodeComp') currencyCodeComp;
  @ViewChild('infoIsinCode') infoIsinCode;
  @ViewChild('newCfiInfo') newCfiInfo;
  @ViewChild('isinCodeComp') isinCodeComp;
  @ViewChild('cficodeComp') cficodeComp;
  
  constructor(private fb: FormBuilder, private router:Router, private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    if(!this.isinSearchServiceService.getEntryUpdateParam()){
      this.router.navigate(['/inquiryEntryUpdate']);
    }

    this.fg = this.fb.group({
      securityId: [],
      securitySymbol: ['', [Validators.required, Validators.maxLength(32)]],
      market: ['', [Validators.required, Validators.maxLength(10)]],
      securityType: ['', [Validators.required, Validators.maxLength(30)]],
      companyCode: ['', [Validators.required, Validators.maxLength(4)]],
      uniqueCodeType: ['', [Validators.required]],
      uniqueCode: ['', [Validators.required, Validators.maxLength(32)]],
      issuerNameThai: ['', [Validators.required, Validators.maxLength(150)]],
      issuerNameEnglish: ['', [Validators.required, Validators.maxLength(150)]],
      securityNameThai: ['', [Validators.required, Validators.maxLength(150)]],
      securityNameEnglish: ['', [Validators.required, Validators.maxLength(150)]],
      parValue: ['', [Validators.required, Validators.maxLength(150)]],
      currencyCode: ['', [Validators.required]],

      unitTrust: [],
      issueDate: ['', [Validators.maxLength(10)]],
      hasForeignLimit: [],
      maturityDate: ['', [Validators.maxLength(10)]],
      nativeCode: ['', [Validators.maxLength(1)]],
      interestRateType: [],
      futureOptionType: [],
      exercisePriceCurrency: ['', [Validators.maxLength(3)]],
      interestRate: ['', [Validators.maxLength(11)]],
      interestRateFrequency: ['', [Validators.maxLength(3)]],
      dwType: [],
      exerciseStyle: [],
      underlyingSymbol: ['', [Validators.maxLength(32)]],

      cfiGroup: ['', [Validators.required, Validators.maxLength(1)]],
      cfiCategory: ['', [Validators.required, Validators.maxLength(1)]],
      cfiAttrDigit3: ['', [Validators.required, Validators.maxLength(1)]],
      cfiAttrDigit4: ['', [Validators.required, Validators.maxLength(1)]],
      cfiAttrDigit5: ['', [Validators.required, Validators.maxLength(1)]],
      cfiAttrDigit6: ['', [Validators.required, Validators.maxLength(1)]],
    });
  }

  ngAfterViewInit() {
    let entryUpdateParam = this.isinSearchServiceService.getEntryUpdateParam();
    if (entryUpdateParam) {
      this.securityId = entryUpdateParam.securityId;
      this.securityAbbr = entryUpdateParam.securityAbbr;
      this.securityType = entryUpdateParam.securityType;
      this.companyCode = entryUpdateParam.companyCode;
      this.companyCodeComp.companyCode = this.companyCode;
      this.companyCodeComp.lookupCompanyByCodeOnBlur();
      this.selectCode = entryUpdateParam.selectCode;
      
      if(this.selectCode=='C'){
        this.securityTypeComp.isDisabled = true;
        this.marketComp.isDisabled = true;
        this.companyCodeComp.isDisabled = true;
      }

      if(this.securityId){
        this.getISINDetaiBySecurityId(this.securityId); 
      }

    }
  }

  private getISINDetaiBySecurityId(securityId){
    this.isinSearchServiceService.getUpdateDetail(securityId)
      .subscribe(results => {
        let result = results[0];

        this.securityAbbr = result.securityAbbr;
        this.marketComp.market = result.marketID;
        this.securityTypeComp.securityType = result.securityTypeID;

        if (this.companyCodeComp) {
          this.companyCode = result.companyID;
          this.companyCodeComp.companyCode = this.companyCode;
          this.companyCodeComp.lookupCompanyByCodeOnBlur();
        }

        if (this.uniqueCodeComp) {
          setTimeout(_ => {
            this.uniqueCodeComp.uniqueCodeType = result.issuerUniqueIDType;
          });
        }

        this.uniqueCode = result.issuerUniqueCode;
        this.issuerNameThai = result.issuerNameTH;
        this.issuerNameEnglish = result.issuerNameEN;
        this.securityNameThai = result.securityNameTH;
        this.securityNameEnglish = result.securityNameEN;
        this.parValue = result.parValue;
        if (this.currencyCodeComp) {
          this.currencyCodeComp.currencyCode = result.currencyCode;
        }
        
        if(this.infoIsinCode) {
          if(this.infoIsinCode.underlyingSymbolComp) {
            this.infoIsinCode.underlyingSymbolComp.underlyingSymbol = result.underlyingAsset;
            this.infoIsinCode.underlyingSymbolComp.underlyingSymbolDesc = result.underlyingNameTH;
          }

          if(this.infoIsinCode.interestRateTypeComp){
            this.infoIsinCode.interestRateTypeComp.interestRateType = result.interestRateType;
          }

          if(this.infoIsinCode.exercisePriceCurrComp){
            this.infoIsinCode.exercisePriceCurrComp.exercisePriceCurrency = result.exercisePriceCurrency;
          }

          if(this.infoIsinCode.exerciseStyleComp){
            //this.infoIsinCode.exerciseStyleComp.exerciseStyle = result.????????????;
          }

          if(this.infoIsinCode.futureOptionTypeComp){
            this.infoIsinCode.futureOptionTypeComp.futureOptionType = result.tfexType;
          }

          this.infoIsinCode.unitTrust = result.unitTrustType;
          this.infoIsinCode.issueDate = result.issueDate;
          this.infoIsinCode.maturityDate = result.maturityDate;
          this.infoIsinCode.native = result.debentureNative;
          this.infoIsinCode.interestRate = result.interestRate;
          this.infoIsinCode.interestRateFreq = result.interestRateFreq;
          this.infoIsinCode.dwType = result.dwType;
        }

        if (this.newCfiInfo) {
          this.newCfiInfo.cfiCategory = result.cfiCategory;
          this.newCfiInfo.cfiGroup = result.cfiGroup;
          this.newCfiInfo.initCFIDigits();
          this.newCfiInfo.cfiAttrDigit3 = result.cfiAttr3;
          this.newCfiInfo.cfiAttrDigit4 = result.cfiAttr4;
          this.newCfiInfo.cfiAttrDigit5 = result.cfiAttr5;
          this.newCfiInfo.cfiAttrDigit6 = result.cfiAttr6;
        }

        if (this.isinCodeComp) {
          this.isinCodeComp.isinCodeLocalOrg = result.isinCodeLocal;
          this.isinCodeComp.isinCodeForeignOrg = result.isinCodeForeign;
          this.isinCodeComp.isinCodeNVDROrg = result.isinCodeNVDR;
          this.isinCodeComp.isinCodeTTFOrg = result.isinCodeTTF;
        }

        if (this.cficodeComp) {
          this.cficodeComp.cfiCodeOrg = result.cfiCode;
          this.cficodeComp.cfiCodeNVDROrg = result.cfiCodeNVDR;
        }

      })
  }

  onInitWhenSecurityTypeReady() {
    if (this.selectCode == 'A' || this.selectCode == 'C') {
       this.newCfiInfo.onInitWhenSecurityTypeReady();
    }
  }

  onSecurityTypeChange(securityType: string) {
    if (this.selectCode == 'A' || this.selectCode == 'I') {
      this.infoIsinCode.showInfoForISIN(securityType);
    }
    if (this.selectCode == 'A' || this.selectCode == 'C') {
      this.newCfiInfo.initCFICategory();
      this.newCfiInfo.initCFIGroup();
    }
  }

  backToInquriryEntryUpdate() {
    this.router.navigate(['/inquiryEntryUpdate']);
  }

  reset() {
    if(this.securityId){
      this.getISINDetaiBySecurityId( this.securityId);
    }else{
      this.marketComp.reset();
      this.securityTypeComp.securityType = this.securityType;
      this.companyCodeComp.companyCode = this.companyCode;
      this.companyCodeComp.lookupCompanyByCodeOnBlur();
      this.uniqueCodeComp.reset();
      this.uniqueCode = "";
      this.issuerNameThai = "";
      this.issuerNameEnglish = "";
      this.securityNameThai = "";
      this.securityNameEnglish = "";
      this.parValue = "";
      this.currencyCodeComp.reset();

      if(this.infoIsinCode) {
        if(this.infoIsinCode.underlyingSymbolComp) {
          this.infoIsinCode.underlyingSymbolComp.underlyingSymbol = "";
          this.infoIsinCode.underlyingSymbolComp.underlyingSymbolDesc = "";
        }

        if(this.infoIsinCode.interestRateTypeComp){
          this.infoIsinCode.interestRateTypeComp.interestRateType = "";
        }

        if(this.infoIsinCode.exercisePriceCurrComp){
          this.infoIsinCode.exercisePriceCurrComp.exercisePriceCurrency = "";
        }

        if(this.infoIsinCode.exerciseStyleComp){
          this.infoIsinCode.exerciseStyleComp.exerciseStyle = "";
        }

        if(this.infoIsinCode.futureOptionTypeComp){
          this.infoIsinCode.futureOptionTypeComp.futureOptionType = "";
        }

        this.infoIsinCode.unitTrust = "";
        this.infoIsinCode.issueDate = "";
        this.infoIsinCode.maturityDate = "";
        this.infoIsinCode.native = "";
        this.infoIsinCode.interestRate = "";
        this.infoIsinCode.interestRateFreq = "";
        this.infoIsinCode.dwType = "";
      }

      this.newCfiInfo.securityType = this.securityType;
      this.newCfiInfo.cfiCategory = "";
      this.newCfiInfo.cfiGroup = "";
      this.newCfiInfo.onInitWhenSecurityTypeReady();
    }
  }

  submit(fg: FormGroup) {
    console.log(">>>> Action Submit");
    console.log('model:', fg.value);
    console.log('isValid:',fg.valid);
    this.isinSearchServiceService.updateISIN(fg.value, "2696");
  }

}
