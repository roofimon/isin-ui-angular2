import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'isin-cfi-code',
  templateUrl: './cfi-code.component.html',
  styleUrls: ['./cfi-code.component.css']
})
export class CfiCodeComponent implements OnInit {

  public cfiCodeOrg: string;
  public cfiCodeNVDROrg: string;
  constructor() { }

  ngOnInit() {
  }

}
