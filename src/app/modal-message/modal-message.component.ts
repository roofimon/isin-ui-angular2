import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/modal';

@Component({
  selector: 'isin-modal-message',
  templateUrl: './modal-message.component.html',
  styleUrls: ['./modal-message.component.css']
})
export class ModalMessageComponent implements OnInit {
  
  @ViewChild('messageModal') messageModal:ModalDirective;
  @ViewChild('modalMessageBody') modalMessageBody:ElementRef;

  public title:string;

  constructor() { }

  ngOnInit() {
    
  }

  show(title:string, content:string) {
    this.title = title;
    this.modalMessageBody.nativeElement.innerHTML = content;
    this.messageModal.show();
  }

}
