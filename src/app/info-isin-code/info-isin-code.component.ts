import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'isin-info-isin-code',
  templateUrl: './info-isin-code.component.html',
  styleUrls: ['./info-isin-code.component.css']
})
export class InfoIsinCodeComponent implements OnInit {

  @Input('parentFormGroup') parentFormGroup: FormGroup;
  @Input() securityType: string;
  @ViewChild('underlyingSymbolComp') underlyingSymbolComp;
  @ViewChild('interestRateTypeComp') interestRateTypeComp;
  @ViewChild('exercisePriceCurrComp') exercisePriceCurrComp;
  @ViewChild('futureOptionTypeComp') futureOptionTypeComp;
  @ViewChild('exerciseStyleComp') exerciseStyleComp;

  public isISINInfoShow: boolean = false;
  public isUnderlyingSymbolShow: boolean = false;
  public isIssueDateShow: boolean = false;
  public isMaturityDateShow: boolean = false;
  public isNativeShow: boolean = false;
  public isInterestRateTypeShow: boolean = false;
  public isInterestRateShow: boolean = false;
  public isInterestRateFrequencyShow: boolean = false;
  public isExercisePriceCurrencyShow: boolean = false;
  public isUnitTrustTypeShow: boolean = false;
  public isHasForeignLimitShow: boolean = false;
  public isDWTypeShow: boolean = false;
  public isExerciseStyleShow: boolean = false;
  public isFutureOptionTypeShow: boolean = false;

  constructor() { }

  ngOnInit() {
    this.showInfoForISIN(this.securityType);
  }

  public showInfoForISIN(securityType: string) {
    this.clearShowHideInfo();
    if (securityType != undefined && securityType != "" && securityType != "01" && securityType != "06" && securityType != "13") {
      this.isISINInfoShow = true;
    }

    if (securityType === "02") {
      this.isUnderlyingSymbolShow = true;
    } else if (securityType === "03" || securityType === "04") {
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
      this.isNativeShow = true;
      this.isInterestRateTypeShow = true;
      this.isInterestRateShow = true;
      this.isInterestRateFrequencyShow = true;
      if (securityType === "04") { this.isUnderlyingSymbolShow = true; }
    } else if (securityType === "12") {
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
      this.isUnderlyingSymbolShow = true;
    } else if (securityType === "05" || securityType === "10") {
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
      this.isUnderlyingSymbolShow = true;
      this.isExercisePriceCurrencyShow = true;
    } else if (securityType === "07") {
      this.isUnitTrustTypeShow = true;
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
    } else if (securityType === "14" || securityType === "17" || securityType === "18") {
      this.isIssueDateShow = true;
      this.isHasForeignLimitShow = true;
    } else if (securityType === "E7") {
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
      this.isUnderlyingSymbolShow = true;
    } else if (securityType === "08") {
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
      this.isUnderlyingSymbolShow = true;
      this.isDWTypeShow = true;
      this.isExerciseStyleShow = true;
    } else if (securityType === "TX") {
      this.isIssueDateShow = true;
      this.isMaturityDateShow = true;
      this.isUnderlyingSymbolShow = true;
      this.isFutureOptionTypeShow = true;
    }
  }

  private clearShowHideInfo() {
    this.isISINInfoShow = false;
    this.isUnderlyingSymbolShow = false;
    this.isIssueDateShow = false;
    this.isMaturityDateShow = false;
    this.isNativeShow = false;
    this.isInterestRateTypeShow = false;
    this.isInterestRateShow = false;
    this.isInterestRateFrequencyShow = false;
    this.isExercisePriceCurrencyShow = false;
    this.isUnitTrustTypeShow = false;
    this.isHasForeignLimitShow = false;
    this.isDWTypeShow = false;
    this.isExerciseStyleShow = false;
    this.isFutureOptionTypeShow = false;
  }

}
