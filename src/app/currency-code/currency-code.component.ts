import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ISINSearchServiceService } from '../isinsearch-service.service';

@Component({
  selector: 'isin-currency-code',
  templateUrl: './currency-code.component.html',
  styleUrls: ['./currency-code.component.css']
})
export class CurrencyCodeComponent implements OnInit {
  
  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  public currencyCode: string;
  results = [];

  constructor(private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    this.listSecurityType();
    this.currencyCode = "THB";
  }

  public listSecurityType(): void {
    this.isinSearchServiceService.listCurrencyCode()
      .subscribe(results => {
        this.results = results;
      });
  }

  reset(){
    this.currencyCode = "THB";
  }

}
