import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ISINSearchServiceService } from '../isinsearch-service.service';

@Component({
  selector: 'isin-security-symbol',
  templateUrl: './security-symbol.component.html',
  styleUrls: ['./security-symbol.component.css']
})
export class SecuritySymbolComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  public securityAbbr: string;
  public securityAbbrDesc: string;
  public results =[];
  columns: Array<any> = [
    {
      title: 'Securities Symbol', name: 'securityAbbr'
    },
    {
      title: 'Market', name: 'marketName'
    },
    {
      title: 'ISIN Code Local', name: 'isinCodeL'
    },
    {
      title: 'Security Name', name: 'securityNameEN'
    }
  ];

  constructor(private isinSearchService: ISINSearchServiceService) { }

  ngOnInit() {
    this.reset();
  }

  reset(){
    this.securityAbbr = "";
    this.securityAbbrDesc = "";
  }

  lookupSecurityByAbbrOnBlur() {
      if(this.securityAbbr !== undefined && this.securityAbbr !== ""){
      this.isinSearchService.lookupSecurityByAbbr(this.securityAbbr)
          .subscribe(results =>{
            this.results = results;
            if(this.results.length == 1){
              this.securityAbbr = this.results[0].securityAbbr;
              this.securityAbbrDesc = this.results[0].securityNameEN;
            }else if(this.results.length > 1){
              //Open Modal
              console.log(this.results);
            }else{
              this.securityAbbr = this.securityAbbr.toUpperCase();
              this.securityAbbrDesc = "Not Found";
            }
          })
      }else{
        this.securityAbbr = "";
        this.securityAbbrDesc = "";
      }
  }

  lookupSecurityByModal(securityType: string){
    this.isinSearchService.lookupSecurityByModal(securityType)
          .subscribe(results => {
            this.results = results;
          });
  }

  clearSearchResultData(){
    this.results = [];
  }

  handleEvent(data: any) {
    this.securityAbbr = data.row.securityAbbr;
    this.securityAbbrDesc = data.row.securityNameEN;
  }

}
