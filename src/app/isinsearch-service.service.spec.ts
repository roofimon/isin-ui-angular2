/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ISINSearchServiceService } from './isinsearch-service.service';

describe('ISINSearchServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ISINSearchServiceService]
    });
  });

  it('should ...', inject([ISINSearchServiceService], (service: ISINSearchServiceService) => {
    expect(service).toBeTruthy();
  }));
});
