import { Component, OnInit, Input, AfterViewInit, AfterViewChecked } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'isin-new-cfi-info',
  templateUrl: './new-cfi-info.component.html',
  styleUrls: ['./new-cfi-info.component.css']
})
export class NewCfiInfoComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  @Input() public securityType: string;
  
  private cfiDigit: string;
  private cfiGroup: string; 
  private cfiCategory: string;
  private cfiAttrDigit3: string;
  private cfiAttrDigit4: string;
  private cfiAttrDigit5: string;
  private cfiAttrDigit6: string;

  results_category = [];
  results_group = [];
  results_digit_3 = [];
  results_digit_4 = [];
  results_digit_5 = [];
  results_digit_6 = [];

  constructor(private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {}

  onInitWhenSecurityTypeReady() {
    this.initCFICategory();
    this.initCFIGroup();
  }

  public initCFICategory() {
    this.results_category = [];
    this.cfiCategory = "";
    if (this.securityType && this.securityType != "") {
      this.isinSearchServiceService.listCFICode(this.securityType, '1', '')
      .subscribe(results => {
        this.results_category = results;
      });
    }
  }

  public initCFIGroup() {
    this.results_group = [];
    this.cfiGroup = "";
    this.resetCFIDigits();
    if (this.securityType && this.securityType != "") {
      this.isinSearchServiceService.listCFICode(this.securityType, '2', '')
      .subscribe(results => {
        this.results_group = results;
      });
    }
  }

  public initCFIDigits() {
    this.resetCFIDigits();
    if (this.securityType && this.securityType != "") {
      this.isinSearchServiceService.listCFICode(this.securityType, '3', this.cfiGroup)
      .subscribe(results => {
        this.results_digit_3 = results;
      });

      this.isinSearchServiceService.listCFICode(this.securityType, '4', this.cfiGroup)
      .subscribe(results => {
        this.results_digit_4 = results;
      });

      this.isinSearchServiceService.listCFICode(this.securityType, '5', this.cfiGroup)
      .subscribe(results => {
        this.results_digit_5 = results;
      });

      this.isinSearchServiceService.listCFICode(this.securityType, '6', this.cfiGroup)
      .subscribe(results => {
        this.results_digit_6 = results;
      });
    }
  }

  private resetCFIDigits(){
    this.results_digit_3 = [];
    this.results_digit_4 = [];
    this.results_digit_5 = [];
    this.results_digit_6 = [];
    this.cfiAttrDigit3 = "";
    this.cfiAttrDigit4 = "";
    this.cfiAttrDigit5 = "";
    this.cfiAttrDigit6 = "";
  }

}
