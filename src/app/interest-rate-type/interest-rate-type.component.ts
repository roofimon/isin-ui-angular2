import { Component, OnInit, Input } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'isin-interest-rate-type',
  templateUrl: './interest-rate-type.component.html',
  styleUrls: ['./interest-rate-type.component.css']
})
export class InterestRateTypeComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup:FormGroup;
  public interestRateType: string;
  results = [];

  constructor(private isinSearchServiceService:ISINSearchServiceService) { }

  ngOnInit() {
    this.listInterestRateType();
    this.interestRateType = "";
  }

  public listInterestRateType() {
    this.isinSearchServiceService.listInterestRateType()
      .subscribe(results => {
        this.results = results;
    });
  }

}
