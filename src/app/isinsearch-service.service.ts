import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { tableData  } from './search-result-data';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class ISINSearchServiceService {
  private API_URL = environment.API_URL;

  private inquiryEntryUpdateUrl = this.API_URL + '/isin/inquiryEntryUpdate';
  private listSecTypehUrl = this.API_URL + '/securityType/list';
  private listMarketUrl = this.API_URL + '/market/list';
  private lookUpSecurityByAbbrUrl = this.API_URL + '/security/lookup/byAbbr';
  private lookUpSecurityByModalUrl = this.API_URL + '/security/lookup';
  private lookupCompanyByCodeUrl = this.API_URL + '/company/lookup/byCode';
  private lookUpCompanyByModalUrl = this.API_URL + '/company/lookup';
  private listUniqueCodeUrl = this.API_URL + '/uniqueCode/list';
  private listCurrencyCodeUrl = this.API_URL + '/currencyCode/list';  
  private lookupUnderlyingUrl = this.API_URL + '/underlying/lookup';
  private listFutureOptionUrl = this.API_URL + '/futureOption/list'; 
  private listInterestRateTypeUrl = this.API_URL + '/interestRateType/list';
  private listDWExerciseStyleUrl = this.API_URL + '/DWExerciseStyle/list';
  private listCFICodeUrl = this.API_URL + '/cfi';
  private isinSearchUpdateDetailUrl = this.API_URL + '/isin/inquiryUpdateDetail';
  private createIsinUrl = this.API_URL + '/isin/create';
  private updateIsinUrl = this.API_URL + '/isin/update';

  public searchResult:Array<any> = tableData;

  private entryUpdateParam: EntryUpdateParam;
  
  constructor(private http: Http) {}

  public getSecurityInfo(securityAbbr:string,securityType:string,companyID:string) {

    return this.http.get(this.inquiryEntryUpdateUrl 
                          + "?securityAbbr=" + securityAbbr
                          + "&securityType=" + securityType
                          + "&companyID=" + companyID)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listSecurityType() {
    return this.http.get(this.listSecTypehUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listMarket() {
    return this.http.get(this.listMarketUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public lookupSecurityByAbbr(securityAbbr:string){
    return this.http.get(this.lookUpSecurityByAbbrUrl + "?securityAbbr=" + securityAbbr)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public lookupSecurityByModal(securityType:string){
    return this.http.get(this.lookUpSecurityByModalUrl + "?securityType=" + securityType)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public lookupCompanyByCode(companyCode:string){
    return this.http.get(this.lookupCompanyByCodeUrl + "?companyCode=" + companyCode)
        .map(res => res.json())
        .catch(this.handleError);
  }
  
  public lookupCompanyByModal(companyName:string) {
    return this.http.get(this.lookUpCompanyByModalUrl + "?companyName=" + companyName)
        .map(res => res.json())
        .catch(this.handleError);
  }

  public listUniqueCode() {
    return this.http.get(this.listUniqueCodeUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listCurrencyCode() {
    return this.http.get(this.listCurrencyCodeUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listFutureOption() {
    return this.http.get(this.listFutureOptionUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listInterestRateType() {
    return this.http.get(this.listInterestRateTypeUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listDWExerciseStyle() {
    return this.http.get(this.listDWExerciseStyleUrl)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public listCFICode(securityType: string, cfiDigit: string, cfiGroup: string) {
    return this.http.get(this.listCFICodeUrl
                        + '?securityType=' + securityType
                        + '&cfiDigit=' + cfiDigit
                        + '&cfiGroup=' + cfiGroup)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public lookUpUnderlying(underlyingSymbol: string, underlyingName: string, securityType: string){
    return this.http.get(this.lookupUnderlyingUrl 
                          + '?underlyingSymbol=' + underlyingSymbol
                          + '&underlyingName=' + underlyingName
                          + '&securityType=' + securityType)
      .map(res => res.json())
      .catch(this.handleError);    
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  public getEntryUpdateParam(): EntryUpdateParam {
    return this.entryUpdateParam;
  }

  public setEntryUpdateParam(entryUpdateParam: EntryUpdateParam) {
    this.entryUpdateParam = entryUpdateParam;
  }

  public getUpdateDetail(securityID:string) {
    return this.http.get(this.isinSearchUpdateDetailUrl 
                          + "?securityID=" + securityID)
      .map(res => res.json())
      .catch(this.handleError);
  }

  public createISIN(obj:any, userId:string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let opts: RequestOptions = new RequestOptions();
    opts.headers = headers;

    // return this.http.post(this.createIsinUrl, JSON.stringify(data), opts)
    // .subscribe((res: Response) => {
    //   console.log(res);
    // });

  }

  public updateISIN(obj: any, userId: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('userId', '2969');

    let opts: RequestOptions = new RequestOptions();
    opts.headers = headers;

    return this.http.post(this.updateIsinUrl, JSON.stringify(obj), opts)
    .subscribe((res: Response) => {
      console.log(res);
    });
  }

  private preventUndefined(data){
    return (data == undefined) ? null: data;
  }

}

export class EntryUpdateParam {
  securityId: number;
  securityAbbr: string;
  securityType: string;
  companyCode: string;
  selectCode: string;
}
