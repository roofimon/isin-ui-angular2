import { Component, OnInit, Input } from '@angular/core';
import { ISINSearchServiceService } from '../isinsearch-service.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'isin-exercise-price-currency',
  templateUrl: './exercise-price-currency.component.html',
  styleUrls: ['./exercise-price-currency.component.css']
})
export class ExercisePriceCurrencyComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  public exercisePriceCurrency: string;
  results = [];

  constructor(private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    this.listSecurityType();
    this.exercisePriceCurrency = "THB";
  }

  public listSecurityType(): void {
    this.isinSearchServiceService.listCurrencyCode()
      .subscribe(results => {
        this.results = results;
      });
  }

}
