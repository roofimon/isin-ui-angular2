import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ISINSearchServiceService  } from '../isinsearch-service.service';

@Component({
  providers: [ISINSearchServiceService],
  selector: 'isin-search-result-table',
  templateUrl: './search-result-table.component.html',
  styleUrls: ['./search-result-table.component.css']
})

export class SearchResultTableComponent implements OnInit {
  @Input() data:Array<any> = [];
  @Input() columns:Array<any> = [];
  @Input() selectableColumns:Array<any> = [];
  @Output() selected: EventEmitter<any> = new EventEmitter();
  selectedRow: any;

  public rows:Array<any> = [];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    // className: ['table-striped', 'table-bordered']
    className: ['table-striped']
  };

  public constructor(private isinSearchServiceService: ISINSearchServiceService) {
    this.length = this.data.length;
  }

  public ngOnInit():void {
    // this.transformSelectable(this.data, this.selectableColumns);
    this.onChangeTable(this.config);
  }

  public changePage(page:any, data:Array<any> = this.data):Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = data;
    this.columns.forEach((column:any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item:any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray:Array<any> = [];

    filteredData.forEach((item:any) => {
      let flag = false;
      this.columns.forEach((column:any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    page.itemsPerPage = parseInt(page.itemsPerPage);
    this.page = page ? page.page : this.page;

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    //this.transformSelectable(this.rows, this.selectableColumns);
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    if (!this.selectableColumns || this.selectableColumns.length == 0 || this.selectableColumns.indexOf(data.column) >= 0) {
      this.selectedRow = data;
      //this.selected.next(this.selectedRow);
      this.selected.emit(data);
    }
  }

  ngOnChanges(changes) {
    this.onChangeTable(this.config, {page: this.page, itemsPerPage: this.itemsPerPage});
  }

  // transformSelectable(rows: Array<any>, selectableColumns: Array<any>) {
  //   let keys;
  //   rows.forEach(row => {
  //     if (!keys) {
  //       keys = Object.keys(row);
  //     }
  //     keys.forEach(key => {
  //       if (selectableColumns.indexOf(key) >= 0) {
  //         row[key+""] = "<a class=\"selectableColumns\">" + row[key+""] + "</a>";
  //       }
  //     })
  //   });
  // }

}
