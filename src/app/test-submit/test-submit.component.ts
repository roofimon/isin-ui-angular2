import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'isin-test-submit',
  templateUrl: './test-submit.component.html',
  styleUrls: ['./test-submit.component.css']
})
export class TestSubmitComponent implements OnInit {

  public fg: FormGroup;

  @ViewChild('companyCodeComp')companyCodeComp;

  constructor(private fb:FormBuilder) { }

  ngOnInit() {
    this.fg = this.fb.group({
      securitySymbol: ['', [<any>Validators.required, <any>Validators.maxLength(32)]],
      securityType: ['', [<any>Validators.required]],
      market: ['', [<any>Validators.required]],
      companyCode: ['', [<any>Validators.required, <any>Validators.maxLength(4)]],
      selectCode: ['', [<any>Validators.required]],
      issuerNameThai: ['', [<any>Validators.required, <any>Validators.maxLength(150)]],
    }); 
  }

  submit(){
    console.log(this.companyCodeComp.companyCode);
  }

  submitFg(fg: FormGroup) {
    console.log('model:', fg.value);
    console.log('isValid:',fg.valid);
    console.log('fg.controls.companyCode:',fg.controls['companyCode'].valid);
    console.log('FG:', fg);
  }

}
