import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { Ng2TableModule } from 'ng2-table';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { ModalModule } from 'ng2-bootstrap/modal';

import { SecuritySymbolComponent } from './security-symbol/security-symbol.component';
import { SecurityTypeComponent } from './security-type/security-type.component';
import { CompanyCodeComponent } from './company-code/company-code.component';
import { SelectCodeComponent } from './select-code/select-code.component';
import { SearchResultTableComponent } from './search-result-table/search-result-table.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { SystemMenuComponent } from './system-menu/system-menu.component';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { AppLogoComponent } from './app-logo/app-logo.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { InquiryEntryUpdateComponent } from './inquiry-entry-update/inquiry-entry-update.component';
import { EntryUpdateComponent } from './entry-update/entry-update.component';
import { MarketComponent } from './market/market.component';
import { UniqueCodeTypeComponent } from './unique-code-type/unique-code-type.component';
import { CurrencyCodeComponent } from './currency-code/currency-code.component';
import { InfoIsinCodeComponent } from './info-isin-code/info-isin-code.component';
import { InterestRateTypeComponent } from './interest-rate-type/interest-rate-type.component';
import { UnderlyingSymbolComponent } from './underlying-symbol/underlying-symbol.component';
import { ExercisePriceCurrencyComponent } from './exercise-price-currency/exercise-price-currency.component';
import { ExerciseStyleComponent } from './exercise-style/exercise-style.component';
import { FutureOptionTypeComponent } from './future-option-type/future-option-type.component';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { IsinCodeComponent } from './isin-code/isin-code.component';
import { CfiCodeComponent } from './cfi-code/cfi-code.component';
import { NewCfiInfoComponent } from './new-cfi-info/new-cfi-info.component';
import { ModalMessageComponent } from './modal-message/modal-message.component';
import { InquiryComponent } from './inquiry/inquiry.component';
import { TestSubmitComponent } from './test-submit/test-submit.component';

const appRoutes: Routes = [
  { path: 'inquiryEntryUpdate', component: InquiryEntryUpdateComponent },
  { path: 'entryUpdate', component: EntryUpdateComponent },
  { path: 'inquiry', component: InquiryComponent },

  { path: 'test', component: TestSubmitComponent }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SecuritySymbolComponent,
    SecurityTypeComponent,
    CompanyCodeComponent,
    SelectCodeComponent,
    SearchResultTableComponent,
    BreadcrumbsComponent,
    TopBarComponent,
    SystemMenuComponent,
    UserPanelComponent,
    AppLogoComponent,
    SideBarComponent,
    SideMenuComponent,
    InquiryEntryUpdateComponent,
    EntryUpdateComponent,
    MarketComponent,
    UniqueCodeTypeComponent,
    CurrencyCodeComponent,
    InfoIsinCodeComponent,
    InterestRateTypeComponent,
    UnderlyingSymbolComponent,
    ExercisePriceCurrencyComponent,
    ExerciseStyleComponent,
    FutureOptionTypeComponent,
    ModalConfirmComponent,
    IsinCodeComponent,
    CfiCodeComponent,
    NewCfiInfoComponent,
    ModalMessageComponent,
    InquiryComponent,
    TestSubmitComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
