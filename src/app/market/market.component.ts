import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ISINSearchServiceService } from '../isinsearch-service.service';

@Component({

  selector: 'isin-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css']
})
export class MarketComponent implements OnInit {
  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  
  public market: string;
  public isDisabled: boolean = false;
  results = [];

  constructor(private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    this.listMarket();
    this.market = "";
  }

  public listMarket(): void {
    this.isinSearchServiceService.listMarket()
      .subscribe(results => {
        this.results = results;
      });
  }

  reset(){
    this.market = "";
  }

}
