import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ISINSearchServiceService } from '../isinsearch-service.service';

@Component({
  selector: 'isin-security-type',
  templateUrl: './security-type.component.html',
  styleUrls: ['./security-type.component.css']
})
export class SecurityTypeComponent implements OnInit {

  @Input('parentFormGroup') public parentFormGroup: FormGroup;
  @Input() public securityType: string;
  @Output() ready: EventEmitter<any> = new EventEmitter();
  public isDisabled: boolean = false;
  results = [];

  constructor(private fb:FormBuilder, private isinSearchServiceService: ISINSearchServiceService) { }

  ngOnInit() {
    if(!this.parentFormGroup) {
      this.parentFormGroup = this.fb.group({
         securityType: ['']
        }); 
    }
    this.reset();
  }

  reset(){
    this.listSecurityType();
    this.securityType = "";
  }

  public listSecurityType(): void {
    this.isinSearchServiceService.listSecurityType()
      .subscribe(results => {
        this.results = results;
        this.ready.emit();
      });
  }



}
