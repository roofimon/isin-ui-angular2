export const tableData: Array<any> = [
 {
            "securitiesAbbr": "BBL",
            "companyName": "BANGKOK BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/25/1975",
            "isinCodeLocal": "TH0001010006",
            "isinCodeForeign": "TH0001010014",
            "isinCodeNVDR": "TH0001010R16",
            "cfiCode": "TH0001010006",
            "cfiCodeNVDR": "TH0001010R16"
        },
        {
            "securitiesAbbr": "BJC",
            "companyName": "BERLI JUCKER PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0002010005",
            "isinCodeForeign": "TH0002010013",
            "isinCodeNVDR": "TH0002010R14",
            "cfiCode": "TH0002010005",
            "cfiCodeNVDR": "TH0002010R14"
        },
        {
            "securitiesAbbr": "SCC",
            "companyName": "THE SIAM CEMENT PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/30/1975",
            "isinCodeLocal": "TH0003010004",
            "isinCodeForeign": "TH0003010012",
            "isinCodeNVDR": "TH0003010R12",
            "cfiCode": "TH0003010004",
            "cfiCodeNVDR": "TH0003010R12"
        },
        {
            "securitiesAbbr": "TGI",
            "companyName": "THAI GLASS INDUSTRIES PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0004010003",
            "isinCodeForeign": "TH0004010011",
            "isinCodeNVDR": "",
            "cfiCode": "TH0004010003",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "AIFT",
            "companyName": "AIG FINANCE (THAILAND) PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0005010002",
            "isinCodeForeign": "TH0005010010",
            "isinCodeNVDR": "TH0005010R17",
            "cfiCode": "TH0005010002",
            "cfiCodeNVDR": "TH0005010R17"
        },
        {
            "securitiesAbbr": "CMBT",
            "companyName": "CARNAUDMETALBOX (THAILAND) PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0006010Z07",
            "isinCodeForeign": "TH0006010Z15",
            "isinCodeNVDR": "TH0006010R15",
            "cfiCode": "TH0006010Z07",
            "cfiCodeNVDR": "TH0006010R15"
        },
        {
            "securitiesAbbr": "DTC",
            "companyName": "DUSIT THANI PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/29/1975",
            "isinCodeLocal": "TH0007010Z05",
            "isinCodeForeign": "TH0007010Z13",
            "isinCodeNVDR": "TH0007010R13",
            "cfiCode": "TH0007010Z05",
            "cfiCodeNVDR": "TH0007010R13"
        },
        {
            "securitiesAbbr": "NFS",
            "companyName": "THANACHART CAPITAL PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0083010007",
            "isinCodeForeign": "TH0083010015",
            "isinCodeNVDR": "TH0083010R14",
            "cfiCode": "TH0083010007",
            "cfiCodeNVDR": "TH0083010R14"
        },
        {
            "securitiesAbbr": "JCC",
            "companyName": "JALAPRATHAN CEMENT PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0008010009",
            "isinCodeForeign": "TH0008010017",
            "isinCodeNVDR": "TH0008010R11",
            "cfiCode": "TH0008010009",
            "cfiCodeNVDR": "TH0008010R11"
        },
        {
            "securitiesAbbr": "SUC",
            "companyName": "SAHA-UNION PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/25/1975",
            "isinCodeLocal": "TH0010010005",
            "isinCodeForeign": "TH0010010013",
            "isinCodeNVDR": "TH0010010R17",
            "cfiCode": "TH0010010005",
            "cfiCodeNVDR": "TH0010010R17"
        },
        {
            "securitiesAbbr": "SSC",
            "companyName": "SERMSUK PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/16/1975",
            "isinCodeLocal": "TH0009010008",
            "isinCodeForeign": "TH0009010016",
            "isinCodeNVDR": "TH0009010R19",
            "cfiCode": "TH0009010008",
            "cfiCodeNVDR": "TH0009010R19"
        },
        {
            "securitiesAbbr": "CTW",
            "companyName": "CHAROONG THAI WIRE & CABLE PUBLIC COMPANY LIMITED",
            "effectiveDate": "09/26/1975",
            "isinCodeLocal": "TH0012010003",
            "isinCodeForeign": "TH0012010011",
            "isinCodeNVDR": "TH0012010R13",
            "cfiCode": "TH0012010003",
            "cfiCodeNVDR": "TH0012010R13"
        },
        {
            "securitiesAbbr": "BMB",
            "companyName": "BANGKOK METROPOLITAN BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0014010001",
            "isinCodeForeign": "TH0014010019",
            "isinCodeNVDR": "TH0014010R19",
            "cfiCode": "TH0014010001",
            "cfiCodeNVDR": "TH0014010R19"
        },
        {
            "securitiesAbbr": "KBANK",
            "companyName": "KASIKORNBANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "02/09/1976",
            "isinCodeLocal": "TH0016010009",
            "isinCodeForeign": "TH0016010017",
            "isinCodeNVDR": "TH0016010R14",
            "cfiCode": "TH0016010009",
            "cfiCodeNVDR": "TH0016010R14"
        },
        {
            "securitiesAbbr": "SCB",
            "companyName": "THE SIAM COMMERCIAL BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0015010000",
            "isinCodeForeign": "TH0015010018",
            "isinCodeNVDR": "TH0015010R16",
            "cfiCode": "TH0015010000",
            "cfiCodeNVDR": "TH0015010R16"
        },
        {
            "securitiesAbbr": "IFCT",
            "companyName": "THE INDUSTRIAL FINANCE CORPORATION OF THAILAND",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0013010002",
            "isinCodeForeign": "TH0013010010",
            "isinCodeNVDR": "TH0013010R11",
            "cfiCode": "TH0013010002",
            "cfiCodeNVDR": "TH0013010R11"
        },
        {
            "securitiesAbbr": "BATA",
            "companyName": "BATA SHOE OF THAILAND PUBLIC COMPANY LIMITED",
            "effectiveDate": "03/10/1976",
            "isinCodeLocal": "TH0017010008",
            "isinCodeForeign": "TH0017010016",
            "isinCodeNVDR": "TH0017010R12",
            "cfiCode": "TH0017010008",
            "cfiCodeNVDR": "TH0017010R12"
        },
        {
            "securitiesAbbr": "AFC",
            "companyName": "ASIA FIBER PUBLIC COMPANY LIMITED",
            "effectiveDate": "09/22/1975",
            "isinCodeLocal": "TH0011010004",
            "isinCodeForeign": "TH0011010012",
            "isinCodeNVDR": "TH0011010R15",
            "cfiCode": "TH0011010004",
            "cfiCodeNVDR": "TH0011010R15"
        },
        {
            "securitiesAbbr": "DTDB",
            "companyName": "DBS THAI DANU BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0018010007",
            "isinCodeForeign": "TH0018010015",
            "isinCodeNVDR": "TH0018010R10",
            "cfiCode": "TH0018010007",
            "cfiCodeNVDR": "TH0018010R10"
        },
        {
            "securitiesAbbr": "TIC",
            "companyName": "THE THAI INSURANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "08/19/1976",
            "isinCodeLocal": "TH0019010006",
            "isinCodeForeign": "TH0019010014",
            "isinCodeNVDR": "TH0019010R18",
            "cfiCode": "TH0019010006",
            "cfiCodeNVDR": "TH0019010R18"
        },
        {
            "securitiesAbbr": "SAFE",
            "companyName": "THE SAFETY INSURANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "03/21/1977",
            "isinCodeLocal": "TH0020010003",
            "isinCodeForeign": "TH0020010011",
            "isinCodeNVDR": "TH0020010R16",
            "cfiCode": "TH0020010003",
            "cfiCodeNVDR": "TH0020010R16"
        },
        {
            "securitiesAbbr": "SCCC",
            "companyName": "SIAM CITY CEMENT PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/18/1977",
            "isinCodeLocal": "TH0021010002",
            "isinCodeForeign": "TH0021010010",
            "isinCodeNVDR": "TH0021010R14",
            "cfiCode": "TH0021010002",
            "cfiCodeNVDR": "TH0021010R14"
        },
        {
            "securitiesAbbr": "GF",
            "companyName": "GENERAL FINANCE & SECURITIES PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0022010001",
            "isinCodeForeign": "TH0022010019",
            "isinCodeNVDR": "",
            "cfiCode": "TH0022010001",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "BAY",
            "companyName": "BANK OF AYUDHYA PUBLIC COMPANY LIMITED",
            "effectiveDate": "09/26/1977",
            "isinCodeLocal": "TH0023010000",
            "isinCodeForeign": "TH0023010018",
            "isinCodeNVDR": "TH0023010R10",
            "cfiCode": "TH0023010000",
            "cfiCodeNVDR": "TH0023010R10"
        },
        {
            "securitiesAbbr": "NC",
            "companyName": "NEWCITY (BANGKOK) PUBLIC COMPANY LIMITED",
            "effectiveDate": "09/30/1977",
            "isinCodeLocal": "TH0024010009",
            "isinCodeForeign": "TH0024010017",
            "isinCodeNVDR": "TH0024010R18",
            "cfiCode": "TH0024010009",
            "cfiCodeNVDR": "TH0024010R18"
        },
        {
            "securitiesAbbr": "CIT",
            "companyName": "CARPETS INTERNATIONAL THAILAND PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0025010008",
            "isinCodeForeign": "TH0025010016",
            "isinCodeNVDR": "",
            "cfiCode": "TH0025010008",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "CSC",
            "companyName": "CROWN SEAL PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/30/1977",
            "isinCodeLocal": "TH0026010007",
            "isinCodeForeign": "TH0026010015",
            "isinCodeNVDR": "TH0026010R13",
            "cfiCode": "TH0026010007",
            "cfiCodeNVDR": "TH0026010R13"
        },
        {
            "securitiesAbbr": "SPI",
            "companyName": "SAHA PATHANA INTER-HOLDING PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/30/1977",
            "isinCodeLocal": "TH0027010006",
            "isinCodeForeign": "TH0027010014",
            "isinCodeNVDR": "TH0027010R11",
            "cfiCode": "TH0027010006",
            "cfiCodeNVDR": "TH0027010R11"
        },
        {
            "securitiesAbbr": "UP",
            "companyName": "UNION PLASTIC PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/30/1977",
            "isinCodeLocal": "TH0029010004",
            "isinCodeForeign": "TH0029010012",
            "isinCodeNVDR": "TH0029010R17",
            "cfiCode": "TH0029010004",
            "cfiCodeNVDR": "TH0029010R17"
        },
        {
            "securitiesAbbr": "VK",
            "companyName": "VIDHAYAKOM PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0030010001",
            "isinCodeForeign": "TH0030010019",
            "isinCodeNVDR": "TH0030010R15",
            "cfiCode": "TH0030010001",
            "cfiCodeNVDR": "TH0030010R15"
        },
        {
            "securitiesAbbr": "UPF",
            "companyName": "UNION PIONEER PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/21/1978",
            "isinCodeLocal": "TH0031010000",
            "isinCodeForeign": "TH0031010018",
            "isinCodeNVDR": "TH0031010R13",
            "cfiCode": "TH0031010000",
            "cfiCodeNVDR": "TH0031010R13"
        },
        {
            "securitiesAbbr": "UI",
            "companyName": "UNION THREAD INDUSTRIES CO., LTD.",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0032010009",
            "isinCodeForeign": "TH0032010017",
            "isinCodeNVDR": "TH0032010R11",
            "cfiCode": "TH0032010009",
            "cfiCodeNVDR": "TH0032010R11"
        },
        {
            "securitiesAbbr": "APSP",
            "companyName": "ALCAN PACKAGING STRONGPACK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0033010008",
            "isinCodeForeign": "TH0033010016",
            "isinCodeNVDR": "TH0033010R19",
            "cfiCode": "TH0033010008",
            "cfiCodeNVDR": "TH0033010R19"
        },
        {
            "securitiesAbbr": "GYT",
            "companyName": "GOODYEAR (THAILAND) PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/30/1978",
            "isinCodeLocal": "TH0034010Z09",
            "isinCodeForeign": "TH0034010Z17",
            "isinCodeNVDR": "TH0034010R17",
            "cfiCode": "TH0034010Z09",
            "cfiCodeNVDR": "TH0034010R17"
        },
        {
            "securitiesAbbr": "SPC",
            "companyName": "SAHA PATHANAPIBUL PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/30/1978",
            "isinCodeLocal": "TH0035010006",
            "isinCodeForeign": "TH0035010014",
            "isinCodeNVDR": "TH0035010R14",
            "cfiCode": "TH0035010006",
            "cfiCodeNVDR": "TH0035010R14"
        },
        {
            "securitiesAbbr": "TCMC",
            "companyName": "THAILAND CARPET MANUFACTURING PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/30/1978",
            "isinCodeLocal": "TH0036010005",
            "isinCodeForeign": "TH0036010013",
            "isinCodeNVDR": "TH0036010R12",
            "cfiCode": "TH0036010005",
            "cfiCodeNVDR": "TH0036010R12"
        },
        {
            "securitiesAbbr": "AYAL",
            "companyName": "AYUDHYA AUTO LEASE PUBLIC COMPANY LOMITED",
            "effectiveDate": "08/09/1978",
            "isinCodeLocal": "TH0037010004",
            "isinCodeForeign": "TH0037010012",
            "isinCodeNVDR": "TH0037010R10",
            "cfiCode": "TH0037010004",
            "cfiCodeNVDR": "TH0037010R10"
        },
        {
            "securitiesAbbr": "UOBT",
            "companyName": "UNITED OVERSEAS BANK (THAI) PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0038010003",
            "isinCodeForeign": "TH0038010011",
            "isinCodeNVDR": "TH0038010R18",
            "cfiCode": "TH0038010003",
            "cfiCodeNVDR": "TH0038010R18"
        },
        {
            "securitiesAbbr": "UOBR",
            "companyName": "UOB RADANASIN BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0039010002",
            "isinCodeForeign": "TH0039010010",
            "isinCodeNVDR": "TH0039010R16",
            "cfiCode": "TH0039010002",
            "cfiCodeNVDR": "TH0039010R16"
        },
        {
            "securitiesAbbr": "ICBCT",
            "companyName": "INDUSTRIAL AND COMMERCIAL BANK OF CHINA (THAI) PCL.",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0040A10Y06",
            "isinCodeForeign": "TH0040A10Y14",
            "isinCodeNVDR": "TH0040010R14",
            "cfiCode": "TH0040A10Y06",
            "cfiCodeNVDR": "TH0040010R14"
        },
        {
            "securitiesAbbr": "CIMBT",
            "companyName": "CIMB THAI BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "11/22/1978",
            "isinCodeLocal": "TH0041010008",
            "isinCodeForeign": "TH0041010016",
            "isinCodeNVDR": "TH0041010R12",
            "cfiCode": "TH0041010008",
            "cfiCodeNVDR": "TH0041010R12"
        },
        {
            "securitiesAbbr": "BMB-P",
            "companyName": "BANGKOK METROPOLITAN BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0014020000",
            "isinCodeForeign": "TH0014020018",
            "isinCodeNVDR": "TH0014020R17",
            "cfiCode": "TH0014020000",
            "cfiCodeNVDR": "TH0014020R17"
        },
        {
            "securitiesAbbr": "BKI",
            "companyName": "BANGKOK INSURANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/07/1978",
            "isinCodeLocal": "TH0042010007",
            "isinCodeForeign": "TH0042010015",
            "isinCodeNVDR": "TH0042010R10",
            "cfiCode": "TH0042010007",
            "cfiCodeNVDR": "TH0042010R10"
        },
        {
            "securitiesAbbr": "TF",
            "companyName": "THAI PRESIDENT FOODS PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/07/1978",
            "isinCodeLocal": "TH0043010006",
            "isinCodeForeign": "TH0043010014",
            "isinCodeNVDR": "TH0043010R18",
            "cfiCode": "TH0043010006",
            "cfiCodeNVDR": "TH0043010R18"
        },
        {
            "securitiesAbbr": "ITF",
            "companyName": "ITF FINANCE AND SECURITIES PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0044010Z08",
            "isinCodeForeign": "TH0044010Z16",
            "isinCodeNVDR": "",
            "cfiCode": "TH0044010Z08",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "BNC",
            "companyName": "ASSET BRIGHT PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/21/1978",
            "isinCodeLocal": "TH0045010004",
            "isinCodeForeign": "TH0045010012",
            "isinCodeNVDR": "TH0045010R13",
            "cfiCode": "TH0045010004",
            "cfiCodeNVDR": "TH0045010R13"
        },
        {
            "securitiesAbbr": "ICC",
            "companyName": "I.C.C. INTERNATIONAL PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/21/1978",
            "isinCodeLocal": "TH0046010Z03",
            "isinCodeForeign": "TH0046010Z11",
            "isinCodeNVDR": "TH0046010R11",
            "cfiCode": "TH0046010Z03",
            "cfiCodeNVDR": "TH0046010R11"
        },
        {
            "securitiesAbbr": "UFM",
            "companyName": "UNITED FLOUR MILL PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/29/1978",
            "isinCodeLocal": "TH0047010002",
            "isinCodeForeign": "TH0047010010",
            "isinCodeNVDR": "TH0047010R19",
            "cfiCode": "TH0047010002",
            "cfiCodeNVDR": "TH0047010R19"
        },
        {
            "securitiesAbbr": "TIG",
            "companyName": "THAI INDUSTRIAL GASES PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0049010000",
            "isinCodeForeign": "TH0049010018",
            "isinCodeNVDR": "TH0049010R15",
            "cfiCode": "TH0049010000",
            "cfiCodeNVDR": "TH0049010R15"
        },
        {
            "securitiesAbbr": "WG",
            "companyName": "WHITE GROUP PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/11/1979",
            "isinCodeLocal": "TH0050010006",
            "isinCodeForeign": "TH0050010014",
            "isinCodeNVDR": "TH0050010R13",
            "cfiCode": "TH0050010006",
            "cfiCodeNVDR": "TH0050010R13"
        },
        {
            "securitiesAbbr": "STC",
            "companyName": "SIAM TYRE PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0048010001",
            "isinCodeForeign": "TH0048010019",
            "isinCodeNVDR": "",
            "cfiCode": "TH0048010001",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "PHATR-O",
            "companyName": "PHATRA THANAKIT PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0051010005",
            "isinCodeForeign": "TH0051010013",
            "isinCodeNVDR": "",
            "cfiCode": "TH0051010005",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "STRD",
            "companyName": "ASIA JOINT PANORAMA PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0052010004",
            "isinCodeForeign": "TH0052010012",
            "isinCodeNVDR": "TH0052010R19",
            "cfiCode": "TH0052010004",
            "cfiCodeNVDR": "TH0052010R19"
        },
        {
            "securitiesAbbr": "PHA",
            "companyName": "PHATRA INSURANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0053010003",
            "isinCodeForeign": "TH0053010011",
            "isinCodeNVDR": "TH0053010R17",
            "cfiCode": "TH0053010003",
            "cfiCodeNVDR": "TH0053010R17"
        },
        {
            "securitiesAbbr": "SS",
            "companyName": "SUNSHINE PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0054010002",
            "isinCodeForeign": "TH0054010010",
            "isinCodeNVDR": "TH0054010R15",
            "cfiCode": "TH0054010002",
            "cfiCodeNVDR": "TH0054010R15"
        },
        {
            "securitiesAbbr": "TGCI",
            "companyName": "THAI-GERMAN CERAMIC INDUSTRY PUBLIC COMPANY LIMITED",
            "effectiveDate": "11/17/1980",
            "isinCodeLocal": "TH0055010001",
            "isinCodeForeign": "TH0055010019",
            "isinCodeNVDR": "TH0055010R12",
            "cfiCode": "TH0055010001",
            "cfiCodeNVDR": "TH0055010R12"
        },
        {
            "securitiesAbbr": "TUPCO",
            "companyName": "THAI UNION PAPER PUBLIC CO.,LTD.",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0057010009",
            "isinCodeForeign": "TH0057010017",
            "isinCodeNVDR": "TH0057010R18",
            "cfiCode": "TH0057010009",
            "cfiCodeNVDR": "TH0057010R18"
        },
        {
            "securitiesAbbr": "KWC",
            "companyName": "KRUNGDHEP SOPHON PUBLIC COMPANY LIMITED",
            "effectiveDate": "11/20/1980",
            "isinCodeLocal": "TH0058010008",
            "isinCodeForeign": "TH0058010016",
            "isinCodeNVDR": "TH0058010R16",
            "cfiCode": "TH0058010008",
            "cfiCodeNVDR": "TH0058010R16"
        },
        {
            "securitiesAbbr": "UT",
            "companyName": "UNION TEXTILE INDUSTRIES PUBLIC COMPANY LIMITED",
            "effectiveDate": "11/20/1980",
            "isinCodeLocal": "TH0059010007",
            "isinCodeForeign": "TH0059010015",
            "isinCodeNVDR": "TH0059010R14",
            "cfiCode": "TH0059010007",
            "cfiCodeNVDR": "TH0059010R14"
        },
        {
            "securitiesAbbr": "TGP",
            "companyName": "THAI GYPSUM PRODUCTS PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0056010000",
            "isinCodeForeign": "TH0056010018",
            "isinCodeNVDR": "TH0056010R10",
            "cfiCode": "TH0056010000",
            "cfiCodeNVDR": "TH0056010R10"
        },
        {
            "securitiesAbbr": "DS",
            "companyName": "DHANA SIAM FINANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0060010004",
            "isinCodeForeign": "TH0060010012",
            "isinCodeNVDR": "",
            "cfiCode": "TH0060010004",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "THL",
            "companyName": "TONGKAH HARBOUR PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/02/1981",
            "isinCodeLocal": "TH0061010Z02",
            "isinCodeForeign": "TH0061010Z10",
            "isinCodeNVDR": "TH0061010R10",
            "cfiCode": "TH0061010Z02",
            "cfiCodeNVDR": "TH0061010R10"
        },
        {
            "securitiesAbbr": "TICO",
            "companyName": "THAI IRYO PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0062010002",
            "isinCodeForeign": "TH0062010010",
            "isinCodeNVDR": "",
            "cfiCode": "TH0062010002",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "TTTM",
            "companyName": "THAI TORAY TEXTILE MILLS PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/26/1981",
            "isinCodeLocal": "TH0063010001",
            "isinCodeForeign": "TH0063010019",
            "isinCodeNVDR": "TH0063010R16",
            "cfiCode": "TH0063010001",
            "cfiCodeNVDR": "TH0063010R16"
        },
        {
            "securitiesAbbr": "TISC_OLD",
            "companyName": "THAI INVESTMENT AND SECURITIES PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0064010000",
            "isinCodeForeign": "TH0064010018",
            "isinCodeNVDR": "",
            "cfiCode": "TH0064010000",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "MCC",
            "companyName": "MULTI-CREDIT CORPORATION OF THAILAND PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0065010009",
            "isinCodeForeign": "TH0065010017",
            "isinCodeNVDR": "",
            "cfiCode": "TH0065010009",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "SCBT",
            "companyName": "STANDARD CHARTERED BANK (THAI) PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0067010007",
            "isinCodeForeign": "TH0067010015",
            "isinCodeNVDR": "TH0067010R17",
            "cfiCode": "TH0067010007",
            "cfiCodeNVDR": "TH0067010R17"
        },
        {
            "securitiesAbbr": "EAC",
            "companyName": "THE EAST ASIATIC (THAILAND) PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0066010008",
            "isinCodeForeign": "",
            "isinCodeNVDR": "",
            "cfiCode": "TH0066010008",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "TMB",
            "companyName": "TMB BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0068010006",
            "isinCodeForeign": "TH0068010014",
            "isinCodeNVDR": "TH0068010R15",
            "cfiCode": "TH0068010006",
            "cfiCodeNVDR": "TH0068010R15"
        },
        {
            "securitiesAbbr": "WACOAL",
            "companyName": "THAI WACOAL PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/23/1983",
            "isinCodeLocal": "TH0069010005",
            "isinCodeForeign": "TH0069010013",
            "isinCodeNVDR": "TH0069010R13",
            "cfiCode": "TH0069010005",
            "cfiCodeNVDR": "TH0069010R13"
        },
        {
            "securitiesAbbr": "BRC",
            "companyName": "BANGKOK RUBBER PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/30/1983",
            "isinCodeLocal": "TH0070010002",
            "isinCodeForeign": "TH0070010010",
            "isinCodeNVDR": "TH0070010R11",
            "cfiCode": "TH0070010002",
            "cfiCodeNVDR": "TH0070010R11"
        },
        {
            "securitiesAbbr": "DTM",
            "companyName": "DATAMAT PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/30/1983",
            "isinCodeLocal": "TH0071010Z01",
            "isinCodeForeign": "TH0071010Z19",
            "isinCodeNVDR": "TH0071010R19",
            "cfiCode": "TH0071010Z01",
            "cfiCodeNVDR": "TH0071010R19"
        },
        {
            "securitiesAbbr": "TPC",
            "companyName": "THAI PLASTIC AND CHEMICALS PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/27/1984",
            "isinCodeLocal": "TH0072010000",
            "isinCodeForeign": "TH0072010018",
            "isinCodeNVDR": "TH0072010R17",
            "cfiCode": "TH0072010000",
            "cfiCodeNVDR": "TH0072010R17"
        },
        {
            "securitiesAbbr": "SINGER",
            "companyName": "SINGER THAILAND PUBLIC COMPANY LIMITED",
            "effectiveDate": "06/28/1984",
            "isinCodeLocal": "TH0073A10Z05",
            "isinCodeForeign": "TH0073A10Z13",
            "isinCodeNVDR": "TH0073010R15",
            "cfiCode": "TH0073A10Z05",
            "cfiCodeNVDR": "TH0073010R15"
        },
        {
            "securitiesAbbr": "UAF",
            "companyName": "UNION ASIA FINANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0074010008",
            "isinCodeForeign": "TH0074010016",
            "isinCodeNVDR": "",
            "cfiCode": "TH0074010008",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "BFIT",
            "companyName": "BANGKOK FIRST INVESTMENT & TRUST PUBLIC CO.,LTD.",
            "effectiveDate": "11/28/1984",
            "isinCodeLocal": "TH0076010006",
            "isinCodeForeign": "TH0076010014",
            "isinCodeNVDR": "TH0076010R18",
            "cfiCode": "TH0076010006",
            "cfiCodeNVDR": "TH0076010R18"
        },
        {
            "securitiesAbbr": "BAP",
            "companyName": "BANGKOK AGRO-INDUSTRIAL PRODUCTS PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0077010005",
            "isinCodeForeign": "TH0077010013",
            "isinCodeNVDR": "TH0077010R16",
            "cfiCode": "TH0077010005",
            "cfiCodeNVDR": "TH0077010R16"
        },
        {
            "securitiesAbbr": "POST",
            "companyName": "THE POST PUBLISHING PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/25/1984",
            "isinCodeLocal": "TH0078A10Z00",
            "isinCodeForeign": "TH0078A10Z18",
            "isinCodeNVDR": "TH0078010R14",
            "cfiCode": "TH0078A10Z00",
            "cfiCodeNVDR": "TH0078010R14"
        },
        {
            "securitiesAbbr": "TR",
            "companyName": "THAI RAYON PUBLIC COMPANY LIMITED",
            "effectiveDate": "09/21/1984",
            "isinCodeLocal": "TH0075010007",
            "isinCodeForeign": "TH0075010015",
            "isinCodeNVDR": "TH0075010R10",
            "cfiCode": "TH0075010007",
            "cfiCodeNVDR": "TH0075010R10"
        },
        {
            "securitiesAbbr": "FFT",
            "companyName": "FOREMOST FRIESLAND (THAILAND) PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0079010003",
            "isinCodeForeign": "TH0079010011",
            "isinCodeNVDR": "",
            "cfiCode": "TH0079010003",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "SFP",
            "companyName": "SIAM FOOD PRODUCTS PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/19/1985",
            "isinCodeLocal": "TH0080010000",
            "isinCodeForeign": "TH0080010018",
            "isinCodeNVDR": "TH0080010R10",
            "cfiCode": "TH0080010000",
            "cfiCodeNVDR": "TH0080010R10"
        },
        {
            "securitiesAbbr": "TWC",
            "companyName": "THAI WAH STARCH PUBLIC COMPANY LIMITED",
            "effectiveDate": "12/16/1985",
            "isinCodeLocal": "TH0081010009",
            "isinCodeForeign": "TH0081010017",
            "isinCodeNVDR": "TH0081010R18",
            "cfiCode": "TH0081010009",
            "cfiCodeNVDR": "TH0081010R18"
        },
        {
            "securitiesAbbr": "SCSMG",
            "companyName": "SAMAGGI INSURANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "09/25/1986",
            "isinCodeLocal": "TH0082010008",
            "isinCodeForeign": "TH0082010016",
            "isinCodeNVDR": "TH0082010R16",
            "cfiCode": "TH0082010008",
            "cfiCodeNVDR": "TH0082010R16"
        },
        {
            "securitiesAbbr": "NFS-P",
            "companyName": "THANACHART CAPITAL PUBLIC COMPANY LIMITED",
            "effectiveDate": "04/29/1975",
            "isinCodeLocal": "TH0083020006",
            "isinCodeForeign": "TH0083020014",
            "isinCodeNVDR": "TH0083020R12",
            "cfiCode": "TH0083020006",
            "cfiCodeNVDR": "TH0083020R12"
        },
        {
            "securitiesAbbr": "FBCB",
            "companyName": "FIRST BANGKOK CITY BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0084010Z04",
            "isinCodeForeign": "TH0084010Z12",
            "isinCodeNVDR": "",
            "cfiCode": "TH0084010Z04",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "FBCB-P",
            "companyName": "FIRST BANGKOK CITY BANK PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0084020Z02",
            "isinCodeForeign": "",
            "isinCodeNVDR": "",
            "cfiCode": "TH0084020Z02",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "SF4",
            "companyName": "SINPINYO FUND IV",
            "effectiveDate": "03/20/1987",
            "isinCodeLocal": "TH0085010005",
            "isinCodeForeign": "TH0085010013",
            "isinCodeNVDR": "",
            "cfiCode": "TH0085010005",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "BAP#4",
            "companyName": "BANGKOK AGRO-INDUSTRIAL PRODUCTS PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/01/1987",
            "isinCodeLocal": "TH0077032702",
            "isinCodeForeign": "TH0077032728",
            "isinCodeNVDR": "",
            "cfiCode": "TH0077032702",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "NEP",
            "companyName": "NEP REALTY AND INDUSTRY PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/16/1987",
            "isinCodeLocal": "TH0086010004",
            "isinCodeForeign": "TH0086010012",
            "isinCodeNVDR": "TH0086010R17",
            "cfiCode": "TH0086010004",
            "cfiCodeNVDR": "TH0086010R17"
        },
        {
            "securitiesAbbr": "FE",
            "companyName": "FAR EAST DDB PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/21/1987",
            "isinCodeLocal": "TH0087010003",
            "isinCodeForeign": "TH0087010011",
            "isinCodeNVDR": "TH0087010R15",
            "cfiCode": "TH0087010003",
            "cfiCodeNVDR": "TH0087010R15"
        },
        {
            "securitiesAbbr": "PDI",
            "companyName": "PADAENG INDUSTRY PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/21/1987",
            "isinCodeLocal": "TH0088010002",
            "isinCodeForeign": "TH0088010010",
            "isinCodeNVDR": "TH0088010R13",
            "cfiCode": "TH0088010002",
            "cfiCodeNVDR": "TH0088010R13"
        },
        {
            "securitiesAbbr": "TNL",
            "companyName": "THANULUX PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/21/1987",
            "isinCodeLocal": "TH0089010001",
            "isinCodeForeign": "TH0089010019",
            "isinCodeNVDR": "TH0089010R11",
            "cfiCode": "TH0089010001",
            "cfiCodeNVDR": "TH0089010R11"
        },
        {
            "securitiesAbbr": "TPCORP",
            "companyName": "TEXTILE PRESTIGE PUBLIC COMPANY LIMITED",
            "effectiveDate": "07/21/1987",
            "isinCodeLocal": "TH0090010008",
            "isinCodeForeign": "TH0090010016",
            "isinCodeNVDR": "TH0090010R19",
            "cfiCode": "TH0090010008",
            "cfiCodeNVDR": "TH0090010R19"
        },
        {
            "securitiesAbbr": "OCC",
            "companyName": "O.C.C. PUBLIC COMPANY LIMITED",
            "effectiveDate": "08/07/1987",
            "isinCodeLocal": "TH0091010007",
            "isinCodeForeign": "TH0091010015",
            "isinCodeNVDR": "TH0091010R17",
            "cfiCode": "TH0091010007",
            "cfiCodeNVDR": "TH0091010R17"
        },
        {
            "securitiesAbbr": "SST",
            "companyName": "SUB SRI THAI PUBLIC COMPANY LIMITED",
            "effectiveDate": "08/07/1987",
            "isinCodeLocal": "TH0092010Y08",
            "isinCodeForeign": "TH0092010Y16",
            "isinCodeNVDR": "TH0092010R15",
            "cfiCode": "TH0092010Y08",
            "cfiCodeNVDR": "TH0092010R15"
        },
        {
            "securitiesAbbr": "AYUD",
            "companyName": "SRI AYUDHYA CAPITAL PUBLIC COMPANY LIMITED",
            "effectiveDate": "08/24/1987",
            "isinCodeLocal": "TH0093010Y06",
            "isinCodeForeign": "TH0093010Y14",
            "isinCodeNVDR": "TH0093010R13",
            "cfiCode": "TH0093010Y06",
            "cfiCodeNVDR": "TH0093010R13"
        },
        {
            "securitiesAbbr": "NAVA",
            "companyName": "NAVA FINANCE PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0094010004",
            "isinCodeForeign": "TH0094010012",
            "isinCodeNVDR": "",
            "cfiCode": "TH0094010004",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "CMIC",
            "companyName": "CMIC FINANCE AND SECURITIES PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/21/2011",
            "isinCodeLocal": "TH0096010002",
            "isinCodeForeign": "TH0096010010",
            "isinCodeNVDR": "TH0096010R16",
            "cfiCode": "TH0096010002",
            "cfiCodeNVDR": "TH0096010R16"
        },
        {
            "securitiesAbbr": "SF5",
            "companyName": "SINPINYO FUND V",
            "effectiveDate": "08/14/1987",
            "isinCodeLocal": "TH0097010001",
            "isinCodeForeign": "TH0097010019",
            "isinCodeNVDR": "",
            "cfiCode": "TH0097010001",
            "cfiCodeNVDR": ""
        },
        {
            "securitiesAbbr": "BTNC",
            "companyName": "BOUTIQUE NEWCITY PUBLIC COMPANY LIMITED",
            "effectiveDate": "10/06/1987",
            "isinCodeLocal": "TH0095010003",
            "isinCodeForeign": "TH0095010011",
            "isinCodeNVDR": "TH0095010R18",
            "cfiCode": "TH0095010003",
            "cfiCodeNVDR": "TH0095010R18"
        }
];